import knex from "knex"
import { hashPassword } from "../hash"

export class UserService {
    constructor(private knex: knex) { }

    async getUsersId(user: number) {
        const result = await this.knex.raw(/*SQL*/`SELECT nickname,icon, "isAdmin" FROM "Users" WHERE id = ?`, [user])
        // console.log(result.rows)
        return result.rows[0]
    }

    async createUser(username: string, password: string, nickname: string, icon: string) {
        const hasedPassword = await hashPassword(password)
        const result = await this.knex.raw(/*SQL*/`INSERT INTO "Users" (username,password,nickname,icon,"isAdmin") VALUES (?,?,?,?,?) RETURNING id`, [username, hasedPassword, nickname, icon, 0])
        const userId = result.rows[0].id
        await this.knex("Portfolio2").insert([
            {
                holding_currency: "HKD",
                amount: 1000000,
                user_id: userId
            },
            {
                holding_currency: "JPY",
                amount: 0,
                user_id: userId
            }, {
                holding_currency: "USD",
                amount: 0,
                user_id: userId
            }, {
                holding_currency: "EUR",
                amount: 0,
                user_id: userId
            }, {
                holding_currency: "CNY",
                amount: 0,
                user_id: userId
            }, {
                holding_currency: "AUD",
                amount: 0,
                user_id: userId
            }, {
                holding_currency: "CAD",
                amount: 0,
                user_id: userId
            },
            {
                holding_currency: "CHF",
                amount: 0,
                user_id: userId
            },
            {
                holding_currency: "TWD",
                amount: 0,
                user_id: userId
            }
        ])

        return userId
    }

    async getUsersName(username: string) {
        const result = await this.knex.raw(/*SQL*/`SELECT username,id FROM "Users" WHERE username = ?`, [username])
        // console.log(result)
        return result.rows[0]
    }


    async saveExchange(userID: number, exchangeID: number) {
        const checkSave = await this.knex.raw(/*SQL*/`select * from "User_save" where user_id = ${userID} AND exchange_id = ${exchangeID}`)
        // console.log(checkSave.rows.length);

        if (checkSave.rows.length === 0) {
            const result = await this.knex.raw(/*SQL*/`INSERT INTO "User_save" (user_id, exchange_id) VALUES (?, ?) RETURNING id;`,
                [userID, exchangeID]
            )
            return result.rows[0].id
        } else {
            return "duplicated"
        }


    }

    async checkExchange(userID: number, exchangeID: number) {

        const checkSave = await this.knex.raw(/*SQL*/`select * from "User_save" where user_id = ${userID} AND exchange_id = ${exchangeID}`)
        if (checkSave.rows.length !== 0) {
            return "saved"
        } else {
            return "notSaved"
        }
    }

    async cancelSave(userID: number, exchangeID: number) {
        const result = await this.knex.raw(/*SQL*/`DELETE from "User_save" where user_id = ${userID} AND exchange_id = ${exchangeID} RETURNING id;`)
        return result.rows;
    }

    async getUserComment(userID: number) {
        const result = await this.knex.raw(/*SQL*/`select "Exchange_comment".*, date_trunc('minute', ("Exchange_comment".created_at)),"Exchanges".name from "Exchange_comment" 
        INNER JOIN "Exchanges" ON "Exchange_comment".exchange_id = "Exchanges".id 
        where "Exchange_comment".user_id = ${userID};`)



        return result.rows;

    }

    async getUserSave(userID: number) {
        const result = await this.knex.raw(/*SQL*/`SELECT "Exchanges".*, "Area".area, "Exchange_image".*, "Image"."Image", ROUND(AVG("Exchange_comment".rating),2)
        from "Exchanges" INNER JOIN "Area" ON "Exchanges".area_id = "Area".id
        LEFT JOIN  "Exchange_image" ON "Exchanges".id = "Exchange_image"."exchange_id"
        LEFT JOIN  "Image" ON "Exchange_image".image_id = "Image".id
        LEFT JOIN  "Exchange_comment" ON "Exchanges".id = "Exchange_comment"."exchange_id"
        INNER JOIN "User_save" ON "Exchanges".id = "User_save".exchange_id
        WHERE "User_save".user_id = ${userID}
        GROUP BY "Exchanges".id,"Exchange_image".ID, "Image"."Image", "Area".area ORDER BY "Exchange_image".id
        ;`)

        return result.rows;
    }

    async acceptDonation(value: number, orderID: string) {
        const result = await this.knex("donation").insert([
            {
                "value": value,
                "orderID": orderID
            }
        ]).returning("id")

        return result[0];
    }

    async donationRecord(donationID:number, userID:number) {
        const result = await this.knex("user_donation").insert([
            {
                "value_id": donationID,
                "user_id": userID
            }
        ]).returning("user_id");
        return result[0];
    }

    async selectDonationRecord(userID:number){
        const result = await this.knex.raw(/*SQL*/`SELECT "donation".value, "donation"."orderID", TO_CHAR("donation"."created_at", 'YYYY-MM-DD HH:MI:SS') from "donation"
        INNER JOIN "user_donation" ON "user_donation".value_id = "donation".id
        WHERE "user_donation".user_id = ${userID};
        `)

        return result.rows;
    }
    
}


