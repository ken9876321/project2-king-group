import knex from "knex";

export class SqlFxDataService {
    constructor(private knex: knex) { }


    async getFxChartData() {
        const minData1 = await this.knex.select().from("Min_chart1");
        const minData2 = await this.knex.select().from("Min_chart2");

        const mergedMinData = minData1.concat(minData2);

        let minFinalResult = {};
        for (let i = 0; i < mergedMinData.length; i++) {
            if (!minFinalResult[mergedMinData[i].from_cur]) {
                minFinalResult[mergedMinData[i].from_cur] = [
                    {
                        x: mergedMinData[i].date,
                        y: [
                            mergedMinData[i].open,
                            mergedMinData[i].high,
                            mergedMinData[i].low,
                            mergedMinData[i].close
                        ]
                    }
                ]
            } else {
                minFinalResult[mergedMinData[i].from_cur].push(
                    {
                        x: mergedMinData[i].date,
                        y: [
                            mergedMinData[i].open,
                            mergedMinData[i].high,
                            mergedMinData[i].low,
                            mergedMinData[i].close
                        ]
                    }
                )
            }
        }


        const dayData1 = await this.knex.select().from("Day_chart1");
        const dayData2 = await this.knex.select().from("Day_chart2");

        const mergedDayData = dayData1.concat(dayData2);

        let dayFinalResult = {};
        for (let i = 0; i < mergedDayData.length; i++) {
            if (!dayFinalResult[mergedDayData[i].from_cur]) {
                dayFinalResult[mergedDayData[i].from_cur] = [
                    {
                        x: mergedDayData[i].date,
                        y: [
                            mergedDayData[i].open,
                            mergedDayData[i].high,
                            mergedDayData[i].low,
                            mergedDayData[i].close
                        ]
                    }
                ]
            } else {
                dayFinalResult[mergedDayData[i].from_cur].push(
                    {
                        x: mergedDayData[i].date,
                        y: [
                            mergedDayData[i].open,
                            mergedDayData[i].high,
                            mergedDayData[i].low,
                            mergedDayData[i].close
                        ]
                    }
                )
            }
        }

        return { minFinalResult, dayFinalResult };
    }

    async getFxQuote() {
        const dayData1 = await this.knex.select().from("Day_chart1");
        const dayData2 = await this.knex.select().from("Day_chart2");

        const mergedDayData = dayData1.concat(dayData2);

        let reverseDayData = [];
        for (let i = 0; i < mergedDayData.length; i++) {
            reverseDayData.unshift(mergedDayData[i])
        }

        const minData1 = await this.knex.select().from("Min_chart1");
        const minData2 = await this.knex.select().from("Min_chart2");

        const mergedMinData = minData1.concat(minData2);

        let reverseMinData = [];
        for (let i = 0; i < mergedMinData.length; i++) {
            reverseMinData.unshift(mergedMinData[i])
        }

        let quoteResult = {};
        for (let i = 0; i < reverseDayData.length; i++) {
            if (!quoteResult[reverseDayData[i].from_cur]) {
                quoteResult[reverseDayData[i].from_cur] =
                {
                    date: reverseMinData[i].date,
                    price: reverseMinData[i].close,
                    difference: ((reverseDayData[i].close / reverseMinData[i + 1].close - 1) * 100).toFixed(3)
                }
            }
        }

        const quoteArray = Object.entries(quoteResult);

        let finalArray = [];

        for (let i in quoteArray) {
            finalArray.unshift(quoteArray[i])
        }

        return finalArray;
    }

}