import knex from "knex"
// import puppeteer from 'puppeteer'

export class ExPageService {
    constructor(private knex: knex) { }

    async getComment(id: number) {
        const comments = await this.knex.raw(/*SQL*/`SELECT "Exchange_comment".*, TO_CHAR("Exchange_comment"."created_at", 'YYYY-MM-DD HH:MI:SS'), "Users".nickname from "Exchange_comment" inner join "Users" ON "Exchange_comment".user_id = "Users".id WHERE "Exchange_comment".exchange_id = ${id};`);
        // console.log(comments.rows);
        return comments.rows
    }

    async insertComment(userId: number, exchangeID: number, rating: number, title: string, content: string) {
        const commentsId = await this.knex.raw(/*SQL*/`INSERT INTO "Exchange_comment" (user_id, exchange_id ,rating ,title, content) 
        VALUES (?,?,?,?,?) RETURNING id`,
            [userId, exchangeID, rating, title, content])

        const IdArr = commentsId.rows[0]

        console.log(`insert Comment id:${IdArr.id}`);
    }

    async getRate(id: number) {
        const result = await this.knex.raw(/*SQL*/`SELECT innerhtml FROM "rateHTML" where exchange_id = ${id};`)
        return result.rows;

    }

    async deleteComment(id: number, userID: number) {
        const users = await this.knex.raw(/*SQL*/`SELECT "isAdmin" FROM "Users" WHERE id = ${userID};`)
        const user = users.rows[0];
        if (user.isAdmin === true){
            const result = await this.knex.raw(/*SQL*/`DELETE FROM "Exchange_comment" WHERE id = ${id} RETURNING id;`)
            console.log(`comment${result.rows[0].id} deleted`);
            return `comment${result.rows[0].id} deleted`
        }else{
            return "you are not admin!";
        }

    }
}

// async getRate(id: string | number) {
//     const links = await this.knex.raw(/*SQL*/`SELECT id,link FROM "Exchanges" where "id" = ${id};`)
//     const linksArr = links.rows
//     // console.log(linksArr[0]["link"]);

//     // const specificObj = linksArr.find( (element:object) => element["id"] == id )
//     // console.log(specificObj.link);

//     const browser = await puppeteer.launch({
//         // headless: false,
//         args: ['–no-sandbox', '–disable-setuid-sandbox'],
//         // ignoreDefaultArgs: [‘–disable-extensions’]
//     });
//     const page = await browser.newPage();
//     await page.goto(`${linksArr[0]["link"]}`);

//     const rateBoard = await page.$(".rate-board")
//     // const aHandle = await page.evaluateHandle(() => document.querySelector(".f4 w-100 center"));
//     const resultHandle = await page.evaluateHandle(body => body.innerHTML, rateBoard);
//     const result = await resultHandle.jsonValue()

//     // console.log(result);


//     await browser.close();
//     return result;
// }