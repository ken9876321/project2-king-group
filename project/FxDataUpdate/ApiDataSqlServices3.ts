import axios from "axios";
import Knex from "knex";

const API_URL = "https://alpha-vantage.p.rapidapi.com/query"
const API_KEY = "d8b6be954bmsh453b3dae7a76ecbp1308a4jsnd13c65da6c59"
const apiConfig = {
    "method": "GET" as const,
    "url": API_URL,
    "headers": {
        "content-type": "application/octet-stream",
        "x-rapidapi-host": "alpha-vantage.p.rapidapi.com",
        "x-rapidapi-key": API_KEY
    }
}

async function getMinChart(fromCur: string, toCur: string, interval: number = 60): Promise<any[]> {
    const response = await axios({
        ...apiConfig,
        "params": {
            "datatype": "json",
            "outputsize": "compact",
            "function": "FX_INTRADAY",
            "to_symbol": `${toCur}`,
            "interval": `${interval}min`,
            "from_symbol": `${fromCur}`
        }
    });

    const priceHistory = response.data[`Time Series FX (${interval}min)`]
    return processData(priceHistory)
}

async function getDayChart(fromCur: string, toCur: string): Promise<any[]> {
    const response = await axios({
        ...apiConfig,
        "params": {
            "datatype": "json",
            "outputsize": "compact",
            "function": "FX_DAILY",
            "to_symbol": `${toCur}`,
            "from_symbol": `${fromCur}`
        }
    });

    const priceHistory = response.data["Time Series FX (Daily)"]
    return processData(priceHistory)
}

const processData = (priceHistory: Object) => {
    const result = Object.entries(priceHistory);
    const reverseData: any[] = [];

    for (let i = 0; i < result.length; i++) {
        reverseData.unshift(result[i])
    }

    const dataArray = [];
    for (let i = 0; i < reverseData.length; i++) {
        dataArray.push([
            new Date(reverseData[i][0]),
            [
                parseFloat(reverseData[i][1]['1. open']),
                parseFloat(reverseData[i][1]['2. high']),
                parseFloat(reverseData[i][1]['3. low']),
                parseFloat(reverseData[i][1]['4. close'])]
        ])
    }

    return dataArray;
}

async function updateChartData(knex: Knex, fxArray: string[][], table: string, 
    callback: (fromCur: string, toCur: string) => Promise<any[]>) {
    console.log(`[info] Updating ${table} chart data!`);
    await knex(table).del();
    const trx = await knex.transaction();
    try {
        for (let i = 0; i < fxArray.length; i++) {
            const sixtyMinData = await callback(fxArray[i][0], fxArray[i][1])
            console.log(sixtyMinData.length)
            for (let j = 0; j < sixtyMinData.length; j++) {
                await trx(table).insert([
                    {
                        from_cur: fxArray[i][0],
                        to_cur: fxArray[i][1],
                        date: sixtyMinData[j][0],
                        open: sixtyMinData[j][1][0],
                        high: sixtyMinData[j][1][1],
                        low: sixtyMinData[j][1][2],
                        close: sixtyMinData[j][1][3]
                    }
                ])
            }
            console.log(`[Info] Finished ${i + 1} Chart data`)
        }
        console.log("[info] Transaction commit");
        await trx.commit();
    } catch (e) {
        console.error(e);
        await trx.rollback();
    }
}

const sleep = (ms: number) => {
    return new Promise((resolve) => {
        setTimeout(resolve, ms);
    });
}

const fxArray1 = [
    ["JPY", "HKD"],
    ["USD", "HKD"],
    ["EUR", "HKD"],
    ["CNY", "HKD"]
];

const fxArray2 = [
    ["AUD", "HKD"],
    ["CAD", "HKD"],
    ["CHF", "HKD"],
    ["TWD", "HKD"]
];

const knexConfig = require("../knexfile");
const knex = Knex(knexConfig[process.env.NODE_ENV || "development"]);

(async() => {
    console.log("[Start]", new Date());
    try {
        await updateChartData(knex, fxArray1, "Min_chart1", getMinChart);
        await sleep(300000);
        await updateChartData(knex, fxArray2, "Min_chart2", getMinChart);
        await sleep(300000);
        await updateChartData(knex, fxArray1, "Day_chart1", getDayChart);
        await sleep(300000);
        await updateChartData(knex, fxArray2, "Day_chart2", getDayChart);
    } catch (e) {
        console.error(e);
    } finally {
        await knex.destroy()
        console.log("[END]", new Date())
    }
})()
