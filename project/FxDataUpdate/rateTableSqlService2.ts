import Knex from "knex";
import puppeteer from 'puppeteer';

const knexConfig = require("../knexfile");
const knex = Knex(knexConfig[process.env.NODE_ENV || "development"]);

console.log("running rateTableSqlService.ts", new Date());

async function capHtml() {
    const result = await knex.raw(/*SQL*/`SELECT id, link FROM "Exchanges";`);
    const linkSArr = result.rows;

    const trx = await knex.transaction();
    await trx("rateHTML").del();
    const browser = await puppeteer.launch({
        // headless: false,
        args: ['–no-sandbox', '–disable-setuid-sandbox','--single-process','--disable-dev-shm-usage'],
        ignoreDefaultArgs: ['–disable-extensions']
    });

    try {
        for (const link of linkSArr) {
            try {
                 const page = await browser.newPage();
                 page.setDefaultNavigationTimeout(300000);
                 await page.goto(`${link.link}`);
 
                 const rateBoard = await page.$(".rate-board")
                 const resultHandle = await page.evaluateHandle(body => body.innerHTML, rateBoard);
                 let result = await resultHandle.jsonValue()
 
                 console.log(typeof (result));
                 await page.close()
                 if(result == null){
                    result = '<div>我們未能提供匯率表，請過後再查詢</div>'
                 }
                 await trx("rateHTML").insert([
                     { exchange_id: link.id, innerhtml: result }
                 ]).returning("id");
            } catch (e) {
                continue;
            }
         }

        console.log("[info]insert html successfully");

        await trx.commit();
        console.log("[info]transactions done");
    } catch (err) {
        console.error(err);
        await trx.rollback();
    } finally {
        await browser.close();
        await knex.destroy();
        console.log("end", new Date())
    }
}




capHtml().then(() => {
    console.log("[End]", new Date().getTime());
  });