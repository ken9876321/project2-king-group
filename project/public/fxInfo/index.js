window.onload = function () {

    let timeButtonControl = [0, 0, 0, 0, 0, 0, 0, 0];
    let timeChoice = ["minFinalResult", "dayFinalResult"]

    async function readChart() {
        console.log("readChart is activated")
        const fetchChartData = await fetch('/fxInfo/');
        const fetchQuoteData = await fetch('/fxInfo/fxQuote/');

        await fetchQuoteData.json().then((quoteData) => {

            document.querySelector('.fx_display1').innerHTML = "";
            document.querySelector('.fx_display2').innerHTML = "";
            document.querySelector('.fx_display3').innerHTML = "";
            document.querySelector('.fx_display4').innerHTML = "";

            let fxDisplay1 = '<div>'

            for (let i = 0; i < 2; i++) {
                fxDisplay1 += `<div class="currency">${quoteData[i][0]}/HKD</div>`;

                fxDisplay1 += `${quoteData[i][1].difference > 0 ?
                    `<div class="up">${quoteData[i][1].price} (+${quoteData[i][1].difference}%)</div>` :
                    `<div class="down">${quoteData[i][1].price} (${quoteData[i][1].difference}%)</div>`}`;
            }

            fxDisplay1 += '</div>'

            document.querySelector('.fx_display1').innerHTML = fxDisplay1;



            let fxDisplay2 = '<div>'

            for (let i = 2; i < 4; i++) {
                fxDisplay2 += `<div class="currency">${quoteData[i][0]}/HKD</div>`;

                fxDisplay2 += `${quoteData[i][1].difference > 0 ?
                    `<div class="up">${quoteData[i][1].price} (+${quoteData[i][1].difference}%)</div>` :
                    `<div class="down">${quoteData[i][1].price} (${quoteData[i][1].difference}%)</div>`}`;
            }

            fxDisplay2 += '</div>'

            document.querySelector('.fx_display2').innerHTML = fxDisplay2;

            let fxDisplay3 = '<div>'

            for (let i = 4; i < 6; i++) {
                fxDisplay3 += `<div class="currency">${quoteData[i][0]}/HKD</div>`;

                fxDisplay3 += `${quoteData[i][1].difference > 0 ?
                    `<div class="up">${quoteData[i][1].price} (+${quoteData[i][1].difference}%)</div>` :
                    `<div class="down">${quoteData[i][1].price} (${quoteData[i][1].difference}%)</div>`}`;
            }

            fxDisplay3 += '</div>'

            document.querySelector('.fx_display3').innerHTML = fxDisplay3;

            let fxDisplay4 = '<div>'

            for (let i = 6; i < 8; i++) {
                fxDisplay4 += `<div class="currency">${quoteData[i][0]}/HKD</div>`;

                fxDisplay4 += `${quoteData[i][1].difference > 0 ?
                    `<div class="up">${quoteData[i][1].price} (+${quoteData[i][1].difference}%)</div>` :
                    `<div class="down">${quoteData[i][1].price} (${quoteData[i][1].difference}%)</div>`}`;
            }

            fxDisplay4 += '</div>'

            document.querySelector('.fx_display4').innerHTML = fxDisplay4;

            document.querySelector('#Timestamp').innerHTML = `更新至美國時間${moment(quoteData[0][1].date).format('MMMM Do YYYY, h:mm:ss a')}`;
        })


        await fetchChartData.json().then((chartData) => {

            const currencies = Object.keys(chartData["minFinalResult"])

            for (let i = 0; i < currencies.length; i++) {

                document.querySelector(`.heading${i + 1}`).innerHTML = `${currencies[i]}/HKD ${timeButtonControl[i] == 0 ? "1 Hour" : "Daily"}`;

                let options = {
                    series: [{
                        data: chartData[timeChoice[timeButtonControl[i]]][currencies[i]]

                    }],
                    chart: {
                        type: 'candlestick',
                        height: 350
                    },
                    title: {
                        text: 'CandleStick Chart',
                        align: 'left'
                    },
                    xaxis: {
                        type: 'datetime'
                    },
                    yaxis: {
                        tooltip: {
                            enabled: true
                        }
                    }
                };
                var chart = new ApexCharts(document.querySelector(`.chart${i + 1}`), options);

                document.querySelector(`.button${i + 1}`).innerHTML = `<button class="hour_button" data-id="${i}">1 Hour</button>` +
                    `<button class="day_button" data-id="${i}">Daily</button>`

                document.querySelector(`.chart${i + 1}`).innerHTML = "";


                chart.render();
            }

            const hourButtons = document.querySelectorAll('.hour_button')
            for (const button of hourButtons) {

                button.addEventListener('click', async (event) => {

                    const x = event.currentTarget;

                    timeButtonControl[x.dataset.id] = 0;

                    readChart();
                })
            }

            const dayButtons = document.querySelectorAll('.day_button')
            for (const button of dayButtons) {

                button.addEventListener('click', async (event) => {

                    const x = event.currentTarget;

                    timeButtonControl[x.dataset.id] = 1;

                    readChart();
                })
            }
        })
    }

    readChart()

    setInterval(function () {
        readChart()
    }, 3600000)

}
