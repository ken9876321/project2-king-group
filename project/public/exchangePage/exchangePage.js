

    let currentPath = window.location.pathname
    //  console.log(currentPath);

    const currentID = currentPath.replace("/exchange/exchangePage/", "")
    //  console.log(currentID);




    async function user() {
        const fetchRes = await fetch('/user')
        const visitor = await fetchRes.json();
    

        //Check if login
        if (visitor.login == false) {
            document.querySelector(".login").innerHTML = '<a class="nav-link" href="/connect/google">Login</a>';
            document.querySelector(".commentButton").addEventListener("click", (event) => {
                document.querySelector("#commentForm").innerHTML = '<div>請先<a href="/connect/google">登入</a>後再留言</div>';
            })
            return;
        }
        document.querySelector(".login").remove();
        document.querySelector(".visitor").innerHTML = `<form action="/user/logout" method="POST"><input type="submit" value="Logout ${visitor.nickname}"></form>`
        document.querySelector(".userIcon").innerHTML = `<a href="/user/userProfile"><img class="image" src=${visitor.icon}></a>`
        


        // comment submit process
        document.querySelector('#commentForm').addEventListener('submit', async (event) => {
            const form = event.currentTarget;
            event.preventDefault();

            const title = document.querySelector('#commentTitle').value
            const rating = document.querySelector('#rateOption').value
            const comment = document.querySelector('#commentInput').value

            if (title.length <= 0 || comment.length <= 0) {
                console.log('error: 你的輸入不可為空白')
                return;
            }

            await fetch('/exchange/comment', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({ "exchangeID": currentID, "title": title, "rating": rating, "comment": comment })
            })

            form.reset();
            location.reload();
        })



    }

    async function gettable() {
        const fetchRes = await fetch('/user')
        const visitor = await fetchRes.json();
        

        // console.log(visitor.icon)
        const fetchExchanges = await fetch(`/exchange/theExchange/${currentID}`);
        const exchangeContents = await fetchExchanges.json();
        console.log(exchangeContents)

        const fetchSave = await fetch(`/user/save/${currentID}`);
        const result = await fetchSave.json();
        console.log(result);

        // check if user Save?
        let save = false;
        if (result.response === "saved") {
            save = true
        }


        document.querySelector(".exchangeDetails").innerHTML = "";
        let exchangeDetailsHTML = ""
        for (let i = 0; i < 1; i++) {
            exchangeDetailsHTML += '<div class="image col-md-3">';
            exchangeDetailsHTML += `<img src="/exchange/exchangeImage/${exchangeContents[i].Image}" alt="">`
            exchangeDetailsHTML += '</div>';

            exchangeDetailsHTML += '<div class="content col-md-8">';
            exchangeDetailsHTML += '<div class="row1">';
            exchangeDetailsHTML += `<div id="title">${exchangeContents[i].name}</div>`

            if (result.response === "saved") {
                exchangeDetailsHTML += '<div id="save">save <button id="saving" class="cancel"><i class="far fa-bookmark"></i></button> </div>';
            } else {
                exchangeDetailsHTML += '<div id="save">save <button id="saving"><i class="far fa-bookmark"></i></button> </div>';
            }
            exchangeDetailsHTML += '</div>'
            exchangeContents[i].round == null ? (exchangeDetailsHTML += `<div id="rating"> 評分： 未有評分/ 5 </div>`) : (exchangeDetailsHTML += `<div id="rating"> 評分：${exchangeContents[i].round}/ 5 </div>`);
            exchangeDetailsHTML += `<div id="Intro"> 介紹：${exchangeContents[i].intro} </div>`
            exchangeDetailsHTML += `<div id="address"> 地址：${exchangeContents[i].address}</div>`
            exchangeDetailsHTML += `<div id="tel"> tel:${exchangeContents[i].tel} </div>`
            exchangeDetailsHTML += '</div>';
        }

        document.querySelector(".exchangeDetails").innerHTML = exchangeDetailsHTML;


        // ****************saveButton******************
        async function saveButton() {

            await fetch(`/user/save/${currentID}`, {
                method: 'POST'
            })

            document.querySelector("#saving").className = "cancel"
            save = true;
        }

        async function cancelButton() {

            await fetch(`/user/save/${currentID}`, {
                method: 'DELETE'
            })
            console.log("canceled!");
            document.querySelector("#saving").className = "saving"
            save = false;

        }

        function saveOrCancel() {
            save ? cancelButton() : saveButton();
        }

        const myButton = document.querySelector("#saving").addEventListener('click', async () => {
            if (visitor.login == false) {
                alert("please login first!");
                return;
            }
            saveOrCancel();

        })





    }

    async function getComment() {
        const fetchRes = await fetch('/user')
        const visitor = await fetchRes.json();
        const fetchComments = await fetch(`/exchange/comments/${currentID}`);
        const comments = await fetchComments.json();

        document.querySelector('.commentCol').innerHTML = "";

        let commentColHTML = ""
        for (const comment of comments) {
            //Displaying the comment
            commentColHTML += `<div class="commentCard">`
            commentColHTML += `<div class="commentTitle">${comment.title}</div>`
            commentColHTML += `<div class="commentRow2">`
            commentColHTML += `<div class="commentUser">用戶：${comment.nickname}</div>`
            commentColHTML += `<div class="commentDate">${comment["to_char"]}</div>`
            commentColHTML += `</div>`
            commentColHTML += `<div class="commentRating">評分：${comment.rating}/5</div>`
            commentColHTML += `<div class="commentContent">${comment.content}</div>`
            //Admin delete button
            if (visitor["isAdmin"] == true){
                commentColHTML += `<button class="cardTrash" data-id=${comment.id}><i class="fas fa-trash-alt"></i></button>`;
            }
            commentColHTML += `</div>`;
        }
        document.querySelector('.commentCol').innerHTML = commentColHTML;

        document.querySelector("#commentOnly").addEventListener('click', (event) => {
            event.preventDefault();
            document.querySelector(".section5").innerHTML = "";
            document.querySelector(".section7").innerHTML = `<div class="container commentCol"></div>`
            getComment();
        })

        if (visitor["isAdmin"] == true){
            const deleteButtons = document.querySelectorAll(".cardTrash");
            for(const deleteButton of deleteButtons){
                deleteButton.addEventListener('click', async(event)=> {
                    const button = event.currentTarget;

                    await fetch(`/exchange/userComment/${button.dataset.id}`,{
                        method: 'DELETE'
                    });
                    getComment();
                })

            }
        }
    }

    async function getRate() {
        document.querySelector('#rateOnly').addEventListener('click', (event) => {
            event.preventDefault();
            document.querySelector(".section7").innerHTML = "";
            document.querySelector(".section5").innerHTML = '<div class="container rate-board"> 載入中，請稍後 <div class="spinner-border" role="status"> <span class="sr-only">Loading...</span> </div> </div>';
            if (rateHTML.response == false) {
                failHTML = "載入失敗，請重新加載"
                document.querySelector('.rate-board').innerHTML = failHTML;
                return;
            }
            
            if(rateHTML[0].innerhtml === undefined){
                document.querySelector('.rate-board').innerHTML = "對不起 我們暫時未能提供匯率，請一會再查詢";
            }
            else{
                document.querySelector('.rate-board').innerHTML = rateHTML[0].innerhtml;
            }
            
        })
     

        const fetchRates = await fetch(`/exchange/rate/${currentID}`);
        const rateHTML = await fetchRates.json();
       
        // console.log(rateHTML);
        if (rateHTML.response == false) {
            failHTML = "載入失敗，請重新加載"
            document.querySelector('.rate-board').innerHTML = failHTML;
            return;
        }

        if(rateHTML[0].innerhtml === undefined){
            document.querySelector('.rate-board').innerHTML = "對不起 我們暫時未能提供匯率，請一會再查詢";
        }
        else{
            document.querySelector('.rate-board').innerHTML = rateHTML[0].innerhtml;
        }


    }

    


window.onload = function () {
    getRate();
    user()
    gettable()
    getComment()
}