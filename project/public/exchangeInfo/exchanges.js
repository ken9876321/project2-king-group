window.onload = function () {
    async function user() {
        const fetchRes = await fetch('/user')
        const visitor = await fetchRes.json()
        // console.log(visitor.icon)


        if (visitor.login == false) {
            // console.log("User didn't login yet");
            document.querySelector(".login").innerHTML = '<a class="nav-link" href="/connect/google">Login</a>';
            return;
        }
        document.querySelector(".visitor").innerHTML = `<form action="/user/logout" method="POST"><input type="submit" value="Logout ${visitor.nickname}"></form>`
        document.querySelector(".userIcon").innerHTML = `<a id="userProfile" href="/user/userProfile"><img class="image" src=${visitor.icon}></a>`
        document.querySelector(".login").remove();

        if (visitor["isAdmin"] == true) {
            document.querySelector(".section4-2").innerHTML =
                (/*html*/`<button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#AddForm"
            aria-expanded="false" aria-controls="collapseExample">
            新增找換店
        </button>

        <form id="AddForm" class="collapse container" action="#">
            <div class="form-group">
                <label for="exchangeName">店鋪名稱</label>
                <input type="text" class="form-control" id="exchangeName" placeholder="標題字數長度為20" name="exchangeName"
                    maxlength="20" required>
            </div>
            <div class="form-group">
                <label for="areaOption">地區</label>
                <select class="form-control" id="areaOption" name="areaOption">
                    <option value="香港">香港</option>
                    <option value="九龍">九龍</option>
                    <option value="新界">新界</option>
                </select>
            </div>

            <div class="form-group">
                <label for="exchangeAddress">店鋪地址</label>
                <input type="text" class="form-control" id="exchangeAddress" placeholder="地址字數長度為50"
                    name="exchangeAddress" maxlength="50" required>
            </div>

            <div class="form-group">
                <label for="exchangeIntro">店鋪簡介</label>
                <textarea class="form-control" id="exchangeIntro" name="exchangeIntro" rows="3" maxlength="100"
                    placeholder="簡介最長字數為100" required></textarea>
            </div>

            <div class="form-group">
                <label for="exchangeLink">店鋪連結</label>
                <input type="text" class="form-control" id="exchangeLink" placeholder="www.google.com"
                    name="exchangeLink" required>
            </div>

            <div class="form-group">
                <label for="exchangeTel">店鋪電話</label>
                <input type="text" class="form-control" id="exchangeTel" placeholder="+852 236000000" name="exchangeTel"
                    maxlength="30" required>
            </div>

            <div class="form-group">
                <label for="exchangeImage">Example file input</label>
                <input type="file" name="photo" class="form-control-file" id="exchangeImage"  accept="image/png, image/jpeg" placeholder="accept png or jpg" required>
            </div>


            <button type="submit" class="btn btn-primary submitButton">Submit</button>

        </form>
`)
        }


        if (visitor["isAdmin"] == true) {
            document.querySelector("#AddForm").addEventListener('submit', async (event) => {
                const form = event.currentTarget;
                event.preventDefault();


                const formData = new FormData(form);
                form.reset();

                await fetch('/exchange/newExchange', {
                    method: 'POST',
                    body: formData
                });

                window.location.reload();

            })
        }
    }




    async function getTable() {
        const fetchExchanges = await fetch('/exchange/getExchanges');
        const exchangeContents = await fetchExchanges.json();

        const fetchRes = await fetch('/user')
        const visitor = await fetchRes.json()
        // console.log(exchangeContents)

        function displayExchanges(exchangeContents) {
            let container = $('#pagination');
            container.pagination({
                dataSource: exchangeContents,
                pageSize: 4,
                callback: function (data, pagination) {
                    document.querySelector(".infoCards").innerHTML = "";
                    let InfoCardsHTML = "";
                    for (let i = 0; i < data.length; i++) {
                        InfoCardsHTML += `<a href="/exchange/exchangePage/${data[i].exchange_id}" class="cardContainer row">`;
                        InfoCardsHTML += '<div class="cardImage col-md-3">';
                        InfoCardsHTML += `<img src="/exchange/exchangeImage/${data[i].Image}" alt="">`;
                        InfoCardsHTML += '</div>';

                        //put the content into exchange container
                        InfoCardsHTML += '<div class="cardContent col-md-8">';
                        InfoCardsHTML += `<h5 class="cardTitle">${data[i].name}</h5>`
                        InfoCardsHTML += `<div class="cardRating"><i class="fas fa-star"></i>：${data[i].round == null ? "-" : data[i].round}</div>`;
                        InfoCardsHTML += `<div class="cardAddress">地址：${data[i].address}</div>`;
                        InfoCardsHTML += `<div class="cardArea">地區：${data[i].area}</div>`;
                        InfoCardsHTML += '</div>';
                        InfoCardsHTML += '</a>';
                    }
                    document.querySelector(".infoCards").innerHTML = InfoCardsHTML;
                }
            })

            // old version
            // document.querySelector(".infoCards").innerHTML = "";
            // let InfoCardsHTML = ""
            // for (let i = 0; i < exchangeContents.length; i++) {
            //     // put the picture into exchange container
            //     InfoCardsHTML += `<a href="/exchange/exchangePage/${exchangeContents[i].exchange_id}" class="cardContainer">`;
            //     InfoCardsHTML += '<div class="cardImage">';
            //     InfoCardsHTML += `<img src="/exchange/exchangeImage/${exchangeContents[i].Image}" alt="">`;
            //     InfoCardsHTML += '</div>';

            //     //put the content into exchange container
            //     InfoCardsHTML += '<div class="cardContent">';
            //     InfoCardsHTML += `<h5 class="cardTitle">${exchangeContents[i].name}</h5>`
            //     InfoCardsHTML += `<div class="cardRating"><i class="fas fa-star"></i>：${exchangeContents[i].round == null ? "-" : exchangeContents[i].round}</div>`;
            //     InfoCardsHTML += `<div class="cardAddress">地址：${exchangeContents[i].address}</div>`;
            //     InfoCardsHTML += `<div class="cardArea">地區：${exchangeContents[i].area}</div>`;
            //     InfoCardsHTML += '</div>';
            //     InfoCardsHTML += '</a>';

            // }
            // console.log(InfoCardsHTML)
            // document.querySelector(".infoCards").innerHTML = InfoCardsHTML;
        }

        displayExchanges(exchangeContents);

        document.querySelector('#All').addEventListener('click', () => {
            displayExchanges(exchangeContents);
        })

        document.querySelector('#HK').addEventListener('click', () => {
            const hkExchanges = exchangeContents.filter(current => current.area == "香港")
            displayExchanges(hkExchanges);
        })

        document.querySelector('#KL').addEventListener('click', () => {
            const hkExchanges = exchangeContents.filter(current => current.area == "九龍")
            displayExchanges(hkExchanges);
        })

        document.querySelector('#NT').addEventListener('click', () => {
            const hkExchanges = exchangeContents.filter(current => current.area == "新界")
            displayExchanges(hkExchanges);
        })
    }



    // function testPagination() {
    //     let container = $('#pagination');
    //     let dataSet = [];
    //     for (let i = 0; i < 195; i++) {
    //         dataSet.push(i);
    //     }
    //     container.pagination({
    //         dataSource: dataSet,
    //         callback: function (data) {
    //             var dataHtml = '<ul>';

    //             for (const item of data) {
    //                 dataHtml += '<li>' + (item + 1) + '</li>';
    //             }

    //             dataHtml += '</ul>';

    //             $("#data-container").html(dataHtml);
    //         }
    //     })
    // }

    document.querySelector('#learnMore').addEventListener('click', (event) => {
        event.preventDefault();
        document.querySelector('.section5').scrollIntoView();
    })

    user();
    getTable()

}








// import  json  from "express";



// window.onload = function () {

//     async function readStock() {
//         const fetchStock = await fetch('/home');
//         const stocks = await fetchStock.json();
//         console.log("readStock is activated")

//     }


// }

// getTable();