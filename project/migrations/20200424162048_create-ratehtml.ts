import * as Knex from "knex";


export async function up(knex: Knex): Promise<any> {
    await knex.schema.createTable("rateHTML", table=> {
        table.increments();
        table.integer("exchange_id").notNullable();
        table.foreign("exchange_id").references("Exchanges.id");
        table.text("innerhtml").notNullable();
        table.timestamps(false, true);
    })
}


export async function down(knex: Knex): Promise<any> {
    await knex.schema.dropTable("rateHTML");
}

