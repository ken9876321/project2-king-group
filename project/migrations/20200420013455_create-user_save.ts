import * as Knex from "knex";


export async function up(knex: Knex): Promise<any> {
    await knex.schema.createTable("User_save", table=> {
        table.increments();
        table.integer("user_id").notNullable();
        table.foreign("user_id").references("Users.id");
        table.integer("exchange_id").notNullable();
        table.foreign("exchange_id").references("Exchanges.id");
    })
}


export async function down(knex: Knex): Promise<any> {
    await knex.schema.dropTable("User_save");
}

