import * as Knex from "knex";


export async function up(knex: Knex): Promise<any> {
    await knex.schema.createTable("Portfolio2", table => {
        table.increments();
        table.string("holding_currency").notNullable();
        table.float("amount").notNullable();
        table.integer("user_id").unsigned();
        table.foreign("user_id").references("Users.id");
        table.timestamps(false, true);
    })

    await knex.schema.createTable("Trading_history", table => {
        table.increments();
        table.string("from_currency").notNullable();
        table.float("from_amount").notNullable();
        table.string("to_currency").notNullable();
        table.float("to_amount", 5, 2).notNullable();
        table.float("fx_rate").notNullable();
        table.integer("user_id").unsigned();
        table.foreign("user_id").references("Users.id");
        table.timestamps(false, true);
    })
}


export async function down(knex: Knex): Promise<any> {
    await knex.schema.dropTable("Portfolio2");
    await knex.schema.dropTable("Trading_history");
}

