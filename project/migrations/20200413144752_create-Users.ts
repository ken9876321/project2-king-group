import Knex from "knex";


export async function up(knex: Knex): Promise<any> {
    const hasTable = await knex.schema.hasTable('Users')
    if (!hasTable) {
        await knex.schema.createTable('Users', table => {
            table.increments();
            table.string("username").notNullable();
            table.string("password").notNullable();
            table.string("nickname");
            table.string("icon");
            table.boolean("isAdmin");
            table.timestamps(false, true);
        })
    }

    const hasTable3 = await knex.schema.hasTable("Portfolio")
    if (!hasTable3) {
        await knex.schema.createTable("Portfolio", table => {
            table.increments();
            table.string("name").notNullable();
            table.string("currency").notNullable();
            table.decimal("amount", 10, 3);
            table.decimal("up_down", 10, 3);
            table.decimal("up_down_percent", 5, 2);
            table.integer("user_id").unsigned();
            table.foreign("user_id").references("Users.id")
            table.timestamps(false, true)
        })
    }

    const hasTable4 = await knex.schema.hasTable("Portfolio_currency")
    if (!hasTable4) {
        await knex.schema.createTable("Portfolio_currency", table => {
            table.increments();
            table.float("amount").notNullable();
            table.string("from_currency").notNullable();
            table.string("to_currency").notNullable();
            table.float("fx_rate");
            table.integer("userID");
            // table.integer("exchange_id").unsigned();
            // table.foreign("exchange_id").references("Exchanges.id");
            table.integer("portfolio_id").unsigned();
            table.foreign("portfolio_id").references("Portfolio.id")
            table.timestamps(false, true);
        })
    }
}


export async function down(knex: Knex): Promise<any> {
    await knex.schema.dropTable('Portfolio_currency')
    await knex.schema.dropTable('Portfolio')
    await knex.schema.dropTable('Users')
}

