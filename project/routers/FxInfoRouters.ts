import { FxInfoService } from '../services/FxInfoServices';
import express from 'express';
import { Request, Response } from 'express';

const fxArray = [
    ["JPY", "HKD"],
    ["USD", "HKD"],
    ["EUR", "HKD"],
    ["CNY", "HKD"],
    ["AUD", "HKD"],
    ["CAD", "HKD"],
    ["CHF", "HKD"]
]

export class FxInfoRouter {

    constructor(private FxInfoService: FxInfoService) { }

    router() {
        const router = express.Router();
        router.get("/daily", this.getDayChart);
        router.get("/MIN", this.getMinChart);
        router.get("/weekly", this.getWeekChart);
        router.get("/quote", this.getQuote);
        router.get("/fx_list", this.getDailyDif);
        return router;
    }

    getDayChart = async (req: Request, res: Response) => {
        try {

            const randomNum = Math.floor(Math.random() * 7)

            const chartData = await this.FxInfoService.getDayChart(fxArray[randomNum][0], fxArray[randomNum][1]);

            res.json([`${fxArray[randomNum][0]}/${fxArray[randomNum][1]}`, chartData]);
        } catch (e) {
            console.error(e);
            res.json({ response: 404 });
        }

    }

    getMinChart = async (req: Request, res: Response) => {
        try {
            const chartData = await this.FxInfoService.getMinChart("JPY", "HKD", 1);

            res.json({ chartData });
        } catch (e) {
            console.error(e);
            res.json({ response: 404 });
        }
    }

    getWeekChart = async (req: Request, res: Response) => {
        try {

            const randomNum = Math.floor(Math.random() * 7)

            const chartData = await this.FxInfoService.getWeekChart(fxArray[randomNum][0], fxArray[randomNum][1]);

            res.json([`${fxArray[randomNum][0]}/${fxArray[randomNum][1]}`, chartData]);
        } catch (e) {
            console.error(e);
            res.json({ response: 404 });
        }
    }

    getQuote = async (req: Request, res: Response) => {
        try {
            const QuoteData = await this.FxInfoService.getQuote("JPY", "HKD");

            res.json({ QuoteData });
        } catch (e) {
            console.error(e);
            res.json({ response: 404 });
        }
    }

    getDailyDif = async (req: Request, res: Response) => {
        try {
            let randomNumArray = []
            randomNumArray.push(Math.floor(Math.random() * 7));

            for (let i = 0; i < 2; i++) {
                let randomNum = Math.floor(Math.random() * 7)
                if (randomNumArray.includes(randomNum)) {
                    i--;
                } else {
                    randomNumArray.push(randomNum);
                }
            }

            const data1 = await this.FxInfoService.getDailyDif(fxArray[randomNumArray[0]][0], fxArray[randomNumArray[0]][1])
            const data2 = await this.FxInfoService.getDailyDif(fxArray[randomNumArray[1]][0], fxArray[randomNumArray[1]][1])
            const data3 = await this.FxInfoService.getDailyDif(fxArray[randomNumArray[2]][0], fxArray[randomNumArray[2]][1])

            res.json([
                [`${fxArray[randomNumArray[0]][0]}/${fxArray[randomNumArray[0]][1]}`, data1],
                [`${fxArray[randomNumArray[1]][0]}/${fxArray[randomNumArray[1]][1]}`, data2],
                [`${fxArray[randomNumArray[2]][0]}/${fxArray[randomNumArray[2]][1]}`, data3]
            ])

        } catch (e) {
            console.error(e);
            res.json({ response: 404 });
        }
    }

}