import Knex from 'knex';
import express from 'express'
import dotenv from "dotenv";
import grant from "grant-express"
import expressSession from "express-session"


import { ExchangeRouter } from './routers/ExchangeRouter';
import { ExchangeService } from './services/ExchangeService';
import { SqlFxDataRouter } from './routers/SqlFxDataRouters';
import { SqlFxDataService } from './services/SqlFxDataServices';
import { UserRouter } from './routers/UserRouter';
import { UserService } from './services/UserService';
import { ExPageRouter } from './routers/ExPageRouter'
import { ExPageService } from './services/ExPageService';
import { TradingRouter } from './routers/TradingRouter';
import { TradingService } from './services/TradingService';
import bodyParser from 'body-parser';;
// import { FxInfoService } from './services/FxInfoServices';
// import { FxInfoRouter } from './routers/FxInfoRouters';
import { isLoggedIn } from "./guards";

dotenv.config()

const knexConfig = require('./knexfile');

const knex = Knex(knexConfig[process.env.NODE_ENV || "development"]);

const app = express();

app.use(expressSession({
    secret: 'Tecky',
    resave: true,
    saveUninitialized: true
}))

// **kinko***************************************
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

const exchangeService = new ExchangeService(knex)
const exchangeRouter = new ExchangeRouter(exchangeService)
const exPageService = new ExPageService(knex)
const exPageRouter = new ExPageRouter(exPageService)
// **********************************************

//**Jerry**********************************
const sqlFxDataService = new SqlFxDataService(knex);
const sqlFxDataRouter = new SqlFxDataRouter(sqlFxDataService);
const tradingService = new TradingService(knex);
const tradingRouter = new TradingRouter(tradingService);
// const fxInfoService = new FxInfoService;
// const fxInfoRouter = new FxInfoRouter(fxInfoService)
//**************************************** 

// **Hayden***************************************
const userService = new UserService(knex);
const userRouter = new UserRouter(userService);
// **********************************************

// app.use(grant({
//     "defaults": {
//         "protocol": "https",
//         "host": "tongstuck.uno",
//         "transport": "session",
//         "state": true,
//     },
//     "google": {
//         "key": process.env.GOOGLE_CLIENT_ID || "",
//         "secret": process.env.GOOGLE_CLIENT_SECRET || "",
//         "scope": ["profile", "email"],
//         "callback": "/user/login/google"
//     },
// }));

app.use(grant({
    "defaults": {
        "protocol": "https",
        "host": process.env.DOMAIN_URL || "",
        "transport": "session",
        "state": true,
    },
    "google": {
        "key": process.env.GOOGLE_CLIENT_ID || "",
        "secret": process.env.GOOGLE_CLIENT_SECRET || "",
        "scope": ["profile", "email"],
        "callback": "/user/login/google"
    },
}));
//**Jerry********************************** 
app.use("/fxInfo", sqlFxDataRouter.router())
// app.use("/fxInfo", fxInfoRouter.router())
app.use("/trading", tradingRouter.router())
//**************************************** 

// **kinko***************************************
app.use("/exchange", exchangeRouter.router())
app.use("/exchange", exPageRouter.router())
// **********************************************

// **Hayden***************************************
app.use("/user", userRouter.router())
// **********************************************

app.use("/", express.static('public/fxInfo'))
app.use("/exchangeInfo", express.static('public/exchangeInfo'))
app.use("/welcome", isLoggedIn, express.static("private/userPage"))
app.use("/images", express.static("picture"))

const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
    console.log(`Listening to the PORT ${PORT}`)
})