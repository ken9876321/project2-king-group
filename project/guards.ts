import { Request, Response, NextFunction } from 'express';

export function isLoggedIn(req: Request, res: Response, next: NextFunction) {
    if (req.session?.user) {
        next();
    } else {
        res.redirect('/');
    }
}

export function isLoggedInToLogin(req: Request, res: Response, next: NextFunction) {
    if (req.session?.user) {
        next();
    } else {
        res.redirect('/connect/google');
    }
}

export function isLoggedForSQL(req: Request, res: Response, next: NextFunction) {
    if (req.session?.user) {
        next();
    } else {
        res.json({ login: false })
    }
}