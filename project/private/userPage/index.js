window.onload = function () {

    let timeButtonControl = [0, 0, 0, 0, 0, 0, 0, 0];
    let timeChoice = ["minFinalResult", "dayFinalResult"]

    async function readChart() {
        console.log("readChart is activated")
        const fetchChartData = await fetch('/fxInfo/');
        const fetchQuoteData = await fetch('/fxInfo/fxQuote/');

        await fetchQuoteData.json().then((quoteData) => {

            document.querySelector('.fx_display1').innerHTML = "";
            document.querySelector('.fx_display2').innerHTML = "";
            document.querySelector('.fx_display3').innerHTML = "";
            document.querySelector('.fx_display4').innerHTML = "";

            let fxDisplay1 = '<div>'

            for (let i = 0; i < 2; i++) {
                fxDisplay1 += `<div class="currency">${quoteData[i][0]}/HKD</div>`;

                fxDisplay1 += `${quoteData[i][1].difference > 0 ?
                    `<div class="up">${quoteData[i][1].price} (+${quoteData[i][1].difference}%)</div>` :
                    `<div class="down">${quoteData[i][1].price} (${quoteData[i][1].difference}%)</div>`}`;
            }

            fxDisplay1 += '</div>'

            document.querySelector('.fx_display1').innerHTML = fxDisplay1;



            let fxDisplay2 = '<div>'

            for (let i = 2; i < 4; i++) {
                fxDisplay2 += `<div class="currency">${quoteData[i][0]}/HKD</div>`;

                fxDisplay2 += `${quoteData[i][1].difference > 0 ?
                    `<div class="up">${quoteData[i][1].price} (+${quoteData[i][1].difference}%)</div>` :
                    `<div class="down">${quoteData[i][1].price} (${quoteData[i][1].difference}%)</div>`}`;
            }

            fxDisplay2 += '</div>'

            document.querySelector('.fx_display2').innerHTML = fxDisplay2;

            let fxDisplay3 = '<div>'

            for (let i = 4; i < 6; i++) {
                fxDisplay3 += `<div class="currency">${quoteData[i][0]}/HKD</div>`;

                fxDisplay3 += `${quoteData[i][1].difference > 0 ?
                    `<div class="up">${quoteData[i][1].price} (+${quoteData[i][1].difference}%)</div>` :
                    `<div class="down">${quoteData[i][1].price} (${quoteData[i][1].difference}%)</div>`}`;
            }

            fxDisplay3 += '</div>'

            document.querySelector('.fx_display3').innerHTML = fxDisplay3;

            let fxDisplay4 = '<div>'

            for (let i = 6; i < 8; i++) {
                fxDisplay4 += `<div class="currency">${quoteData[i][0]}/HKD</div>`;

                fxDisplay4 += `${quoteData[i][1].difference > 0 ?
                    `<div class="up">${quoteData[i][1].price} (+${quoteData[i][1].difference}%)</div>` :
                    `<div class="down">${quoteData[i][1].price} (${quoteData[i][1].difference}%)</div>`}`;
            }

            fxDisplay4 += '</div>'

            document.querySelector('.fx_display4').innerHTML = fxDisplay4;

            document.querySelector('#Timestamp').innerHTML = `更新至${moment(quoteData[0][1].date).format('MMMM Do YYYY, h:mm:ss a')}`;
        })


        await fetchChartData.json().then((chartData) => {

            const currencies = Object.keys(chartData["minFinalResult"])

            for (let i = 0; i < currencies.length; i++) {

                document.querySelector(`.heading${i + 1}`).innerHTML = `${currencies[i]}/HKD ${timeButtonControl[i] == 0 ? "1 Hour" : "Daily"}`;

                let options = {
                    series: [{
                        data: chartData[timeChoice[timeButtonControl[i]]][currencies[i]]

                    }],
                    chart: {
                        type: 'candlestick',
                        height: 350
                    },
                    title: {
                        text: 'CandleStick Chart',
                        align: 'left'
                    },
                    xaxis: {
                        type: 'datetime'
                    },
                    yaxis: {
                        tooltip: {
                            enabled: true
                        }
                    }
                };
                var chart = new ApexCharts(document.querySelector(`.chart${i + 1}`), options);

                document.querySelector(`.button${i + 1}`).innerHTML = `<button class="hour_button" data-id="${i}">1 Hour</button>` +
                    `<button class="day_button" data-id="${i}">Daily</button>`

                document.querySelector(`.chart${i + 1}`).innerHTML = "";


                chart.render();
            }

            const hourButtons = document.querySelectorAll('.hour_button')
            for (const button of hourButtons) {

                button.addEventListener('click', async (event) => {

                    const x = event.currentTarget;

                    timeButtonControl[x.dataset.id] = 0;

                    readChart();
                })
            }

            const dayButtons = document.querySelectorAll('.day_button')
            for (const button of dayButtons) {

                button.addEventListener('click', async (event) => {

                    const x = event.currentTarget;

                    timeButtonControl[x.dataset.id] = 1;

                    readChart();
                })
            }
        })
    }

    async function user() {
        const fetchRes = await fetch('/user')
        const visitor = await fetchRes.json()

        if (visitor.login == false) {
            console.log("User didn't login yet");
            return;
        }

        document.querySelector(".visitor").innerHTML = `<form action="/user/logout" method="POST"><input type="submit" value="Logout ${visitor.nickname}"></form>`
        document.querySelector(".userIcon").innerHTML = `<a id="userProfile" href="/user/userProfile"><img class="image" src=${visitor.icon}></a>`

    }

    async function tradingFx() {
        const trade = document.querySelector(".bt-trade")
        trade.addEventListener("click", () => {
            document.querySelector(".row-2").innerHTML =
                `<div class = panel-2>
                <div class="form-group">
                    <form id="tradeInfo" action="/trading/order" method="POST">
                        <label for="fromCurrency">Sell Currency:</label>
                        <select class="form-control" name="from_cur" id = "fromCurrency">
                            <option value="HKD">HKD</option>
                            <option value="USD">USD</option>
                            <option value="EUR">EUR</option>
                            <option value="CNY">CNY</option>
                            <option value="AUD">AUD</option>
                            <option value="CAD">CAD</option>
                            <option value="CHF">CHF</option>
                            <option value="TWD">TWD</option>
                            <option value="JPY">JPY</option>
                        </select>
                        <label for="buyAmount">Amount:</label>
                        <input type="number" name="from_amount" step="0.01" class="form-control" id="buyAmount"/>
                    
                    
                        <label for="toCurrency">Buy Currency:</label>
                        <select class="form-control" name="to_cur" id = "toCurrency">
                            <option value="HKD">HKD</option>
                            <option value="USD">USD</option>
                            <option value="EUR">EUR</option>
                            <option value="CNY">CNY</option>
                            <option value="AUD">AUD</option>
                            <option value="CAD">CAD</option>
                            <option value="CHF">CHF</option>
                            <option value="TWD">TWD</option>
                            <option value="JPY">JPY</option>
                        </select>
                        <label for="sellAmount">Amount:</label>
                        <input type="number" step="0.01" class="form-control" id="sellAmount"/>
                        <button type="submit" class="btn btn-primary formSubmit">Trade</button> 
                    </form>
                </div>
            </div>`
            const from = document.querySelector("#fromCurrency");
            const to = document.querySelector("#toCurrency");
            const buyAmount = document.querySelector("#buyAmount")
            const sellAmount = document.querySelector("#sellAmount")

            buyAmount.addEventListener('keyup', (event) => {
                onTradeChange()
            })

            sellAmount.addEventListener('keyup', (event) => {
                onTradeChange()
            })

            const onTradeChange = async () => {
                const fromCcyValue = from.value
                const toCcyValue = to.value
                const bAmount = buyAmount.value;

                if (fromCcyValue == toCcyValue) {

                    return;

                } else if (fromCcyValue == "HKD" && toCcyValue !== "") {
                    const fetchExchangeRate = await fetch('/fxInfo/fxQuote')

                    const exchangeRate = await fetchExchangeRate.json();

                    const rate = exchangeRate.filter(rates => rates[0] == toCcyValue)

                    document.querySelector(`#sellAmount`).value = (bAmount / rate[0][1]["price"]).toFixed(2)
                } else if (fromCcyValue !== "" && toCcyValue == "HKD") {
                    try {
                        const fetchExchangeRate = await fetch('/fxInfo/fxQuote')

                        const exchangeRate = await fetchExchangeRate.json();

                        const rate = exchangeRate.filter(rates => rates[0] == fromCcyValue)

                        document.querySelector(`#sellAmount`).value = (bAmount * rate[0][1]["price"]).toFixed(2)
                    } catch (e) {
                        console.error(e)
                    }
                } else if (fromCcyValue !== "HKD" && toCcyValue !== "HKD") {
                    try {

                        const fetchExchangeRate = await fetch('/fxInfo/fxQuote')

                        const exchangeRate = await fetchExchangeRate.json();

                        const rate1 = exchangeRate.filter(rates => rates[0] == fromCcyValue)
                        const rate2 = exchangeRate.filter(rates => rates[0] == toCcyValue)
                        const convertedRate = rate1[0][1]["price"] / rate2[0][1]["price"];

                        document.querySelector(`#sellAmount`).value = (bAmount * convertedRate).toFixed(2)
                    } catch (e) {
                        console.error(e)
                    }
                }
            }

            document.querySelector("#tradeInfo").addEventListener("submit",async (e)=>{
                e.preventDefault()
                const from_cur = document.querySelector("#fromCurrency").value
                const to_cur = document.querySelector("#toCurrency").value
                const from_amount = document.querySelector("#buyAmount").value

                const result = await fetch('/trading/order',{
                    method:"POST",
                    headers:{
                        "content-Type":"application/json"
                    },
                    body:JSON.stringify({from_cur,to_cur,from_amount})
                })
                const noCurrency = await result.json()
                console.log(result)

                if (result.status === 403) {
                    this.alert(noCurrency.message)
                }else if(result.status === 200) {
                    window.location.reload();
                }
            })
        })
    }

    const status = async () => {
        const result = await fetch('/trading/portfolio_status')
        const fetchRes = await result.json()

        for (let i of fetchRes) {

            if (i['amount'] > 0) {
                document.querySelector('.holdingInfo').innerHTML +=
                    `<tr>
                        <td> ${i['holding_currency']} </td>
                        <td> ${i['amount']} </td>
                        <td> ${moment(i['update_date']).format('MMMM Do YYYY, h:mm:ss a')} </td>
                    </tr>`
            }
        }
    }

    const history = async () => {
        const showHistory = document.querySelector(".bt-history")
        showHistory.addEventListener("click", async () => {
            document.querySelector(".row-2").innerHTML =
                `<table class="table">
                <thead>
                        <tr>
                            <th>Update_at</th>
                            <th>From_currency</th>
                            <th>From_amount</th>
                            <th>Fx_rate</th>
                            <th>To_currency</th>
                            <th>At_amount</th>
                        </tr>
                </thead>
                <tbody class="showHistory"></tbody>
            </table>`

            const result = await fetch('/trading/history');
            const fetchRes = await result.json();

            if (fetchRes.length == 0) {
                document.querySelector(".showHistory").innerHTML =
                `<tr>
                    <td>NO HISTORY</td>
                </tr>`
            }

            for (let i of fetchRes) {

                document.querySelector(".showHistory").innerHTML +=
                    `<tr>
                    <td> ${moment(i["updated_at"]).format('MMMM Do YYYY, h:mm:ss a')} </td>
                    <td> ${i["from_currency"]} </td>
                    <td> ${i["from_amount"]} </td>
                    <td> ${i["fx_rate"]} </td>
                    <td> ${i["to_currency"]} </td>
                    <td> ${i["to_amount"]} </td>
                </tr>`
            }
        })
    }

    document.querySelector(".bt-port").addEventListener("click", async () => {
        document.querySelector(".row-2").innerHTML =
            `<table class="table">
            <thead>
                <tr>
                    <th>Holding_Currency</th>
                    <th>Amount</th>
                    <th>Update_Date</th>
                </tr>
            </thead>
            <tbody class="holdingInfo"></tbody>
        </table>`
        await status()
    })

    const resetRecord = document.querySelector(".bt-reset")
    resetRecord.addEventListener("click", async () => {
        const choice = this.confirm("Are you sure reset all your record?")
        if (choice == true) {
            await fetch('/trading/restart', { method: 'POST' })
        } else {
            console.log("cancel")
        }
    })

    // for datatable
    // $(document).ready(function() {
    //     var table = $('#example').DataTable( {
    //         responsive: true
    //     } );
     
    //     new $.fn.dataTable.FixedHeader( table );
    // } );
    // for datatable

    history()
    status()
    readChart()
    user()
    tradingFx()

    setInterval(function () {
        readChart()
    }, 3600000)
}